using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLevel2Mng : MonoBehaviour
{
    public SceneLevel2Mng Instance { get; private set; }
    [SerializeField] private Transform SpawnPlayerLv2;
    private bool checkInitPlayerLoadScene = false;

    private void Awake()
    {
        Instance = this;
    }
    private void CheckPlayerLoad()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level 2") && !checkInitPlayerLoadScene)
        {
            checkInitPlayerLoadScene = true;
            GameObject.FindGameObjectWithTag("Player").gameObject.transform.position = SpawnPlayerLv2.position;
        }
    }
    private void Update()
    {
        CheckPlayerLoad();
    }

}
