﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerControllerTest : MonoBehaviour
{
    #region Variables
    public static PlayerControllerTest Instance;
    public enum State { Run_Left = 0, Run_Right = 1, Jump = 2, Idle = 3, Fall = 4, Attack = 5 }
    private static string NAMEANIM_RUN = "Run";

    private static string NAMEANIM_JUMP = "Jump";
    private static string NAMEANIM_FALL = "Fall";

    public float speed;
    public float jumpForce;
    public float moveInput;
    public int attackDame = 20;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public Animator anim;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public float attackRate = 2f;


    private Rigidbody2D rig2D;
    private bool isGrounded;
    private bool isJumping = false;
    private bool isFalling = false;
    bool canAtack = true;

    private bool canDash = true;
    private bool isDashing;
    private float dashingPower = 100f;
    private float dashingTime = 0.2f;
    [SerializeField] private float dashingCooldown = 1f;
    private bool CheckGround = true;


    public int level = 0;
    public StatCtrl statCtrl;
    public SpawnLayer spawn;
    public GameObject gameOverUI;

    public int maxHealth = 2000;
    public int currentHealth = 0;
    public int maxMana = 200;
    public int currentMana = 0;

    float nextAttackTime = 0.5f;

    [Header("Dash")]
    [SerializeField] private float _velocityDash;
    [SerializeField] private float _timeDash;

    private float _gravedadInicial; //gravity
    private bool _puedeHacerDash = true;
    private bool _sePuedeMove = true;

    [SerializeField] private AudioSource attackSoundEffect;
    public GameCtroler gamectroler;
    //SetAvtiveBoss
    public bool playerinZoneBoss = false;
    public static event Action OnPlayerDeath;


    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        rig2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        SetTrangthai();
        _gravedadInicial = rig2D.gravityScale;

        statCtrl.SetMaxHealth(maxHealth);
        statCtrl.SetMaxMana(maxMana);

    }

    private void SetTrangthai()
    {
        currentHealth = maxHealth;
        currentMana = maxMana;
    }

    void Update()
    {

        isGrounded = IsGround();

        if (isDashing)
        {
            return;
        }
        CheckEvent();
    }

    private void CheckEvent()
    {

        InputSystem();

        if (_sePuedeMove && !anim.GetBool("isDead"))
        {
            Look();
            rig2D.velocity = new Vector2(moveInput * speed, rig2D.velocity.y);
            anim.SetFloat("Speed", math.abs(rig2D.velocity.x));
        }
    }
    private bool IsGround()
    {
        // Kiểm tra player có đang ở trên mặt đất hay không
        bool isGrounded = Physics2D.Raycast(transform.position, Vector2.down, 1.2f, groundLayer) && CheckGround;

        return isGrounded;
    }

    private void Look()
    {
        if (moveInput > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (moveInput < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

    }

    private void ActiveCheckGround()
    {
        CheckGround = true;
    }
    #region Anim Handle Crtl
    private IEnumerator Die()
    {
        anim.SetBool("isDead", true);
        yield return new WaitForSeconds(2f);
        gameOverUI.SetActive(true);
        GameCtroler.Instance.GameOver();
        

    }

    private void Jump()
    {
        rig2D.AddForce(Vector2.up * jumpForce);
        isJumping = true;
        anim.Play(NAMEANIM_JUMP);
    }

    private void Fall()
    {
        if (GetComponent<Rigidbody2D>().velocity.y < 0 && !isFalling)
        {
            isFalling = true;
            anim.SetBool("isFalling", true);
        }
    }

    IEnumerator Attack()
    {
        attackSoundEffect.Play();
        anim.SetTrigger("Attack");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (var item in hitEnemies)
        {
            if (item.gameObject.tag == "Enemy")
            {
                item.GetComponent<EnemyMovement>().TakeDamage(attackDame);
                Debug.Log("hit!!!" + item.gameObject.name);

            }

            if (item.gameObject.tag == "Boss")
            {
                Debug.Log("Atack " + item.gameObject.name);
                if (item.gameObject.TryGetComponent(out BossBehavior bossLevel1))
                {
                    bossLevel1.TakeDamage(attackDame);
                }
                if (item.gameObject.TryGetComponent(out BossPromaxCtrl bossLevel2))
                {
                    bossLevel2.TakeDamage(attackDame);
                }

            }
        }
        yield return new WaitForSeconds(nextAttackTime);
        canAtack = true;
    }

    public IEnumerator TakeDame(int dame)
    {
        yield return new WaitForSeconds(0.8f);


        currentHealth -= dame;
        Debug.Log(currentHealth + "cai nay la ienum take dame");
        statCtrl.SetHealth(currentHealth);
        if (currentHealth > 0)
        {

            anim.SetTrigger("Hurt");
        }
        else
        {
            StartCoroutine(Die());
            Die();
        }
    }


    public void TakeDameImg(int dame)
    {
        currentHealth -= dame;
        Debug.Log(currentHealth + "Cai nay la take dame img");
        statCtrl.SetHealth(currentHealth);
        if (currentHealth > 0)
        {

            anim.SetTrigger("Hurt");
        }
        else
        {
            Die();
        }
    }

    private IEnumerator Dash()
    {
        canDash = false;
        isDashing = true;
        float originalGravity = rig2D.gravityScale;
        rig2D.gravityScale = 0f;
        rig2D.velocity = new Vector2(transform.localScale.x * dashingPower, 0f);
        yield return new WaitForSeconds(dashingTime);
        rig2D.gravityScale = originalGravity;
        isDashing = false;
        yield return new WaitForSeconds(dashingCooldown);
        Debug.Log("======> dc dassh tiep");
        canDash = true;
    }


    private IEnumerator Dash1()
    {
        _sePuedeMove = false; //k cho di chuyen khi dang dash
        _puedeHacerDash = false;
        rig2D.gravityScale = 0;
        rig2D.velocity = new Vector2(_velocityDash * transform.localScale.x, 0f);

        anim.SetBool("Dash1", true);

        yield return new WaitForSeconds(_timeDash);

        _sePuedeMove = true;

        rig2D.gravityScale = _gravedadInicial;
        anim.SetBool("Dash1", false);

    }

    #endregion

    private void InputSystem()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        if (isGrounded)
        {
            //check roi
            anim.SetBool("isFalling", false);
            isFalling = false;

            _puedeHacerDash = true;

            // cham dat
            if (isJumping)
            {
                // Nếu player đã kết thúc việc nhảy, đặt isJumping về false
                rig2D.velocity = new Vector2(rig2D.velocity.x, 0);
                isJumping = false;
            }


            else if (Input.GetKeyDown(KeyCode.J) && canAtack)
            {
                // Bắt đầu thực hiện animation attack
                canAtack = false;
                StartCoroutine(Attack());

            }


            if (Input.GetKeyDown(KeyCode.Space) && !isJumping && _puedeHacerDash)
            {
                Jump();
                CheckGround = false;
                Invoke(nameof(ActiveCheckGround), 0.2f);
            }
            if (Input.GetKeyDown(KeyCode.B) && canDash)
            {
                StartCoroutine(Dash());
            }

        }
        else
        {
            Fall();
        }
        if (Input.GetKeyDown(KeyCode.L) && _puedeHacerDash)
        {
            StartCoroutine(Dash1());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.tag + " ====>");
        if (collision.tag == "item")
        {
            Debug.Log("Dung Item!!!!!!!!!");
            var itemtemp = collision.GetComponent<Items>();
            switch (itemtemp.ItemType)
            {
                case ItemType.none:
                    break;
                case ItemType.health:
                    if(currentHealth < maxHealth) currentHealth += itemtemp.itemValue;
                    statCtrl.SetHealth(currentHealth);
                    break;
                case ItemType.mana:
                    if(currentMana < maxMana) currentMana += itemtemp.itemValue;
                    statCtrl.SetMana(currentMana);
                    break;
                case ItemType.vipproitem:
                    currentHealth = maxHealth;
                    currentMana = maxMana;
                    statCtrl.SetHealth(currentHealth);
                    statCtrl.SetMana(currentMana);
                    break;
                default:
                    break;
            }
            Destroy(itemtemp.gameObject);
        }

        else if (collision.tag == "BossActiveZone")
        {
            Debug.Log("in Boss " + tag);
            playerinZoneBoss = true;
        }

        
        if (collision.tag == "Deadzone")
        {
            StartCoroutine(Die());
            Die();
        }
    }

    public IEnumerator Respawn(float duration)
    {
        yield return new WaitForSeconds(duration);
        transform.position = Vector2.zero;
        currentHealth = maxHealth;
        currentMana = maxMana;
        statCtrl.SetMana(maxMana);
        statCtrl.SetHealth(maxHealth);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }


}
