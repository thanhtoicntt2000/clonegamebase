using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatCtrl : MonoBehaviour
{
    public Slider sliderHP;
    public Slider sliderMana;

    public Gradient gradient_hp;
    public Gradient gradient_mana;

    public Image fillHP;
    public Image fillMana;

    private void Awake()
    {
       
            DontDestroyOnLoad(this.gameObject);
       
    }
    public void SetMaxHealth(int health)
    {
        sliderHP.maxValue = health;
        sliderHP.value = health;

        fillHP.color = gradient_hp.Evaluate(1f);
    }

    public void SetMaxMana(int mana)
    {
        sliderMana.maxValue = mana;
        sliderMana.value = mana;
        fillMana.color = gradient_mana.Evaluate(1f);
    }

    public void SetHealth(int health)
    {
        sliderHP.value = health;
        fillHP.color = gradient_hp.Evaluate(sliderHP.normalizedValue);

    }

    public void SetMana(int mana)
    {
        sliderMana.value = mana;
        fillMana.color = gradient_mana.Evaluate(sliderHP.normalizedValue);
    }

    //private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    //{
    //    if (scene.buildIndex == 2 || scene.buildIndex == 1)
    //    {
    //        gameObject.SetActive(true);
    //    }
    //    else
    //    {
    //        gameObject.SetActive(false);
    //    }
    //}
}
