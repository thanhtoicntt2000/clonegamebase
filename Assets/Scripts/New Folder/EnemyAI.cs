﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    #region Public Variables
    public Animator anim;
    public Transform rayCast;
    public LayerMask raycastMask;
    public float raycastLength;
    public float attackDistance;//distance to attack
    public float moveSpeed = 5;
    public float timer; //attack cooldown
    public Transform leftPoint; // Vị trí điểm bên trái của quãng đường di chuyển
    public Transform rightPoint; // Vị trí điểm bên phải của quãng đường di chuyển
    #endregion

    #region Private Variables
    private RaycastHit2D hit;
    private GameObject target;
    
    private float distance;
    private bool attackMode;
    private bool inRange;
    private bool cooling;
    private float intTimer;
    private bool CheckStatusAI = false;
    private bool movingRight = true;
    #endregion

    void Awake()
    {
        intTimer = timer;
        anim = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {
        if (inRange)
        {
            hit = Physics2D.Raycast(rayCast.position, Vector2.left, raycastLength, raycastMask);
            RaycastDebugger();
        }

        if (hit.collider != null)
        {
            EnemyLogic();
        }
        else if (hit.collider == null)
        {
            inRange = false;
        }

        if (inRange == false)
        {
            anim.SetBool("canRun", false);
            StopAttack();
        }
    }

    private void OnTriggerEnter2D(Collider2D trig)
    {
        if (trig.gameObject.tag == "Player")
        {
            target = trig.gameObject;
            inRange = true;
        }
    }

    void EnemyLogic()
    {
        distance = Vector2.Distance(transform.position, target.transform.position);

        if (distance > attackDistance)
        {
            Move();
            StopAttack();
        }
        else if (attackDistance >= distance && cooling == false)
        {
            Attack();
        }

        if (cooling)
        {
            anim.SetBool("Attack", false);
        }
    }

    void Move()
    {
        if (!CheckStatusAI)
        {
            if (movingRight)
            {
                // Di chuyển quái vật sang phải đến vị trí điểm bên phải
                transform.position = Vector3.MoveTowards(transform.position, rightPoint.position, moveSpeed * Time.deltaTime);
                // Nếu quái vật đã đi đến vị trí điểm bên phải, quay đầu lại và di chuyển sang trái
                if (transform.position.x >= rightPoint.position.x)
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = false;
                }
            }
            else
            {
                // Di chuyển quái vật sang trái đến vị trí điểm bên trái
                transform.position = Vector3.MoveTowards(transform.position, leftPoint.position, moveSpeed * Time.deltaTime);
                // Nếu quái vật đã đi đến vị trí điểm bên trái, quay đầu lại và di chuyển sang phải
                if (transform.position.x <= leftPoint.position.x)
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = true;
                }
            }
        }
    }

    void Attack()
    {
        timer = intTimer; // reset timer when player enter attack range
        attackMode = true; // check enemy attack or not

        anim.SetBool("canRun", false);
        anim.SetBool("Attack", true);
    }

    void StopAttack()
    {
        cooling = false;
        attackMode = false;
        anim.SetBool("Attack", false);
    }

    void RaycastDebugger()
    {
        if (distance > attackDistance)
        {
            Debug.DrawRay(rayCast.position, Vector2.left * raycastLength, Color.red);
        }

        else if (distance < attackDistance)
        {
            Debug.DrawRay(rayCast.position, Vector2.left * raycastLength, Color.blue);
        }
    }
}
