using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRun : StateMachineBehaviour
{
    public Transform player;
    public Rigidbody2D rig;
    public float speed = 10f;
    public float attackRange = 2;

    private float delayAttack = 0;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rig = animator.GetComponent<Rigidbody2D>();
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //flip
        if (player.GetComponent<PlayerControllerTest>().playerinZoneBoss)
        {
            if (animator.transform.position.x > player.position.x)
            {
                animator.gameObject.transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                animator.gameObject.transform.localScale = new Vector3(-1, 1, 1);
            }

            Vector2 target = new Vector2(player.position.x, rig.position.y);
            Vector2 newPos = Vector2.MoveTowards(rig.position, target, speed * Time.deltaTime);
            rig.MovePosition(newPos);

            delayAttack -= Time.deltaTime;

            if (Vector2.Distance(player.position, rig.position) <= attackRange)
            {
                animator.SetTrigger("Attack");
                //hit
            }
        }

    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Attack");
    }



}
