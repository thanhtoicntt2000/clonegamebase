using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    none,
    health,
    mana,
    vipproitem
}
public class Items : MonoBehaviour
{
    [SerializeField] private ItemType _itemType;
    [SerializeField] private SpriteRenderer _spriterenderer;
    [SerializeField] private Sprite[] _listsrpites;

    int ItemValue = 0;

    void Start()
    {
        SetItemValue();
    }

    public int itemValue { get => ItemValue; }
    public ItemType ItemType { get => _itemType; set => _itemType = value; }

    private void SetItemValue()
    {
        switch (ItemType)
        {
            case ItemType.none:
                ItemValue = 0;
                break;
            case ItemType.health:
                ItemValue = 50;
                _spriterenderer.sprite = _listsrpites[0];
                break;
            case ItemType.mana:
                ItemValue = 50;
                _spriterenderer.sprite = _listsrpites[1];
                break;
            case ItemType.vipproitem:
                ItemValue = 999;
                _spriterenderer.sprite = _listsrpites[2];
                break;
        }
    }
  
}
