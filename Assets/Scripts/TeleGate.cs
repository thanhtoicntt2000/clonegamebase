﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleGate : MonoBehaviour
{
    PlayerControllerTest player;

    private void LoadSceneLevel()
    {
        SceneManager.LoadScene("Level 2");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.tag + "trigger");
        if (collision.gameObject.tag == "Player") LoadSceneLevel();
    }
}

