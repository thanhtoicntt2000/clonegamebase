using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BossPromax : StateMachineBehaviour
{
    private Transform player;
    private Rigidbody2D rig;
    public float speed = 10f;
    public float attackRange = 2;

    float timeCanAttackSmash;
    float timeCanAttackCast;
    float timeCanAtackFire;

    int countAtack = 0;
    string animname = "";


    float dellayAtack = 2;

    //  OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rig = animator.GetComponent<Rigidbody2D>();
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (player == null) player = GameObject.FindGameObjectWithTag("Player").transform;
        if (rig == null) rig = animator.GetComponent<Rigidbody2D>();
        if (dellayAtack > 0)
        {
            dellayAtack -= Time.deltaTime;
        }
        if (animator.transform.position.x > player.position.x)
        {
            animator.gameObject.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            animator.gameObject.transform.localScale = new Vector3(-1, 1, 1);
        }
        Vector2 target = new Vector2(player.position.x, rig.position.y);
        Vector2 newPos = Vector2.MoveTowards(rig.position, target, speed * Time.deltaTime);
        rig.MovePosition(newPos);

        if (Vector2.Distance(player.position, rig.position) <= attackRange + 2 && dellayAtack <= 0)//3 offset
        {
            dellayAtack = 2;
            Debug.Log(countAtack + " ====");
            switch (countAtack)
            {

                case 1:
                    countAtack++;
                    animator.SetTrigger("Attack");
                    animname = "Attack";
                    break;
                case 2:
                    countAtack++;
                    animator.SetTrigger("AttackSmash");
                    animname = "AttackSmash";
                    break;
                case 3:
                    countAtack++;
                    animator.SetTrigger("Attack");
                    animname = "Attack";
                    break;
                case 4:
                    countAtack++;
                    animator.SetTrigger("AttackCast");
                    animname = "AttackCast";
                    break;
                case 5:
                    countAtack++;
                    animator.SetTrigger("Attack");
                    animname = "Attack";
                    break;
                case 6:
                    countAtack++;
                    animator.SetTrigger("AtackFire");
                    animname = "AtackFire";
                    break;
                case 7:
                    countAtack = 0;
                    animator.SetTrigger("Attack");
                    animname = "Attack";
                    break;
                default:
                    animator.SetTrigger("Attack");
                    animname = "Attack";
                    if (animator.GetBool("Transform") == true) countAtack++;
                    break;
            }

            //hit
        }
        //}
    }

    //  OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger(animname);
    }



}
