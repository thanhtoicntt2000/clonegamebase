﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossBehavior : MonoBehaviour
{
    #region Variables
    public Rigidbody2D rig;
    public Animator anim;
    public SpellDame Cast;
    public int TimeCast = 2;
    float dellaySpell = 0;
    public int dameBoss = 20;
    public int maxHP = 200;
    private int currentHP = 0;
    private float delayAttack = 0;
    public bool Status = false;
    public Transform attackPoint;
    public float attackRange = 2;
    public float speed = 5;

    public HeathCtrl _HeathCtrl;
    public GameCtroler _GameCtroler;
    public LayerMask playerLayers;
    public Collider2D activityArea;

    public GameObject teleGate;

    public GameObject _prefabItem;
    public Transform parentInitItem;


    int tempTimeSpel = 0;


    #endregion

    private void Start()
    {
        currentHP = maxHP;
        _HeathCtrl.SetHeath(currentHP, maxHP);
        _GameCtroler = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCtroler>();
        teleGate.SetActive(false);
        dellaySpell = 0;
        tempTimeSpel = TimeCast;
    }

    public void Hit()
    {

        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayers);
        foreach (var item in hitPlayer)
        {
            Debug.Log("hit!!!" + item.gameObject.name);
            StartCoroutine(item.GetComponent<PlayerControllerTest>().TakeDame(dameBoss));
        }
    }


    void Update()
    {

        delayAttack -= Time.deltaTime;

        if (PlayerControllerTest.Instance.playerinZoneBoss)
        {
            dellaySpell += Time.deltaTime;

            if (dellaySpell >= tempTimeSpel)
            {
                anim.SetTrigger("Cast");
                CastDame();
            }
        }
    }

    public void CastDame()
    {

        Cast.ActiveSpell();
        dellaySpell = 0;
    }


    public void TakeDamage(int dame)
    {
        currentHP -= dame;
        anim.SetTrigger("Hurt");
        _HeathCtrl.SetHeath(currentHP, maxHP);
        if (currentHP <= 0)
        {
            Die();
            Debug.Log($"DIE!!!!");
        }
    }
    public void Die()
    {
        anim.SetBool("Die", true);
        teleGate.SetActive(true);
        StartCoroutine(DestroyAfterDelay(1.1f));

        Items itemtemp = Instantiate(_prefabItem, parentInitItem).GetComponent<Items>();
        itemtemp.transform.localPosition = transform.localPosition;
        itemtemp.ItemType = ItemType.vipproitem;
        }
    private IEnumerator DestroyAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        this.gameObject.SetActive(false);
    }
    private void Attack(PlayerControllerTest player)
    {
        anim.SetTrigger("Attack");
        StartCoroutine(player.TakeDame(dameBoss));
    }

    public void Flip(Transform TransformPlayer)
    {
        Debug.Log(" ====> test flip");
        if (transform.position.x > TransformPlayer.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

}
