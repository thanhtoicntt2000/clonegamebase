using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeathCtrl : MonoBehaviour
{
    public Slider Slider;
    public Color Low;
    public Color Hight;
    public Vector3 offset;
    private void Update()
    {
        Slider.transform.position = Camera.main.WorldToScreenPoint(transform.parent.position + offset);
    }
    public void SetHeath(float heath, float maxHeath)
    {
        Slider.gameObject.SetActive(heath < maxHeath);
        Slider.value = heath;
        Slider.maxValue = maxHeath;
        Slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(Low, Hight, Slider.normalizedValue);
    }
}
