﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    public float speed = 15f;
    public float lifetime = 5f;
    public int damageSpell = 50;

    private Rigidbody2D rb;
    public Vector2 dir;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, lifetime);
    }

    private void FixedUpdate()
    {
        rb.velocity = dir * speed;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            EnemyMovement enemy = other.GetComponent<EnemyMovement>();
            if (enemy != null)
            {
                enemy.TakeDamage(damageSpell);
            }
            Destroy(gameObject);
        }

        if (other.CompareTag("Boss"))
        {
            BossBehavior boss = other.GetComponent<BossBehavior>();
            BossPromaxCtrl boss2 = other.GetComponent<BossPromaxCtrl>();
            if (boss != null)
            {
                boss.TakeDamage(damageSpell);
            }
            else if (boss2 != null)
            {
                boss2.TakeDamage(damageSpell);
            }
            Destroy(gameObject);
        }


    }
}
