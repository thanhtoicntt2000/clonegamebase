﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float moveSpeed; // Tốc độ di chuyển của quái vật
    public float moveDistance; // Khoảng cách di chuyển của quái vật
    public Transform leftPoint; // Vị trí điểm bên trái của quãng đường di chuyển
    public Transform rightPoint; // Vị trí điểm bên phải của quãng đường di chuyển
    private bool movingRight = true; // Xác định quái vật đang di chuyển sang phải hay không
    public Rigidbody2D ridEnemy;
    public bool Checkflowmove = false;
    public int maxHP = 100;
    int currentHP = 0;
    public Animator anim;
    public float attackRange = 1f;
    public int dameEnemy = 10;

    public LayerMask enemyLayers;
    public Transform attackPoint;
    private float dellayAtack = 0;
    public HeathCtrl _HeathCtrl;
    public GameCtroler _GameCtroler;
    public GameObject _prefabItem;
    public Transform parentInitItem;

    private void Start()
    {
        currentHP = maxHP;
        _HeathCtrl.SetHeath(currentHP, maxHP);
        _GameCtroler = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCtroler>();
    }


    public void TakeDamage(int dame)
    {
        currentHP -= dame;
        anim.SetTrigger("Hurt");
        _HeathCtrl.SetHeath(currentHP, maxHP);
        if (currentHP <= 0)
        {
            Die();
            Debug.Log($"DIE!!!!");
        }
    }

    public void Die()
    {
        anim.SetBool("IsDead", true);
        StartCoroutine(DestroyAfterDelay(1f));
        int randomTypeItem = Random.Range(1, 3);
        Debug.Log(randomTypeItem);
        Items itemtemp = Instantiate(_prefabItem, parentInitItem).GetComponent<Items>();
        itemtemp.transform.localPosition = transform.localPosition;
        if (randomTypeItem == 1)
        {
            itemtemp.ItemType = ItemType.health;
        }
        else
        {
            itemtemp.ItemType = ItemType.mana;
        }
    }

    private IEnumerator DestroyAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        this.gameObject.SetActive(false);
        Invoke("SpoiAi", 3f);
    }

    private void SpoiAi()
    {
        this.gameObject.SetActive(true);
        RessetAI();
    }

    private void RessetAI()
    {
        currentHP = maxHP;
        _HeathCtrl.SetHeath(currentHP, maxHP);
    }
    private void checkPlayerCollider()
    {
        if (PlayerControllerTest.Instance == null) return;
        float detecDistancePlayer = Vector2.Distance(PlayerControllerTest.Instance.transform.position, transform.position);
        if (detecDistancePlayer > attackRange * 2.5f || PlayerControllerTest.Instance.currentHealth < 0)
        {
            Checkflowmove = false;
            return;
        }
        else if (detecDistancePlayer <= attackRange)
        {
            if (dellayAtack <= 0 && PlayerControllerTest.Instance.currentHealth > 0)
            {
                dellayAtack = 2;

                Atack(PlayerControllerTest.Instance);
            }
            Flip(PlayerControllerTest.Instance.transform);
        }
        else if (detecDistancePlayer <= attackRange * 2.5f)
        {
            Checkflowmove = true;

            transform.position = Vector3.MoveTowards(transform.position, PlayerControllerTest.Instance.transform.position, moveSpeed * Time.deltaTime);
            Flip(PlayerControllerTest.Instance.transform);
        }


    }
    void Update()
    {
        //if (_GameCtroler.ExitGame) return;
        dellayAtack -= Time.deltaTime;
        checkPlayerCollider();
        if (!Checkflowmove)
        {
            if (movingRight)
            {
                // Di chuyển quái vật sang phải đến vị trí điểm bên phải
                transform.position = Vector3.MoveTowards(transform.position, rightPoint.position, moveSpeed * Time.deltaTime);
                transform.localScale = rightPoint.localScale;
                // Nếu quái vật đã đi đến vị trí điểm bên phải, quay đầu lại và di chuyển sang trái
                if (transform.position.x >= rightPoint.position.x)
                {
                    movingRight = false;
                }
            }
            else
            {
                // Di chuyển quái vật sang trái đến vị trí điểm bên trái
                transform.position = Vector3.MoveTowards(transform.position, leftPoint.position, moveSpeed * Time.deltaTime);
                // Nếu quái vật đã đi đến vị trí điểm bên trái, quay đầu lại và di chuyển sang phải
                transform.localScale = leftPoint.localScale;
                if (transform.position.x <= leftPoint.position.x)
                {
                    movingRight = true;
                }
            }
        }
    }


    // Phát hiện khi người chơi đi vào khu vực của quái vật

    //khi thoát khỏi zone, bỏ follow
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Checkflowmove = false;
        }
    }

    public void Flip(Transform TransformPlayer)
    {

        if (transform.position.x > TransformPlayer.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
    int i = 0;
    private void Atack(PlayerControllerTest player)
    {
        anim.SetTrigger("Atack");

        StartCoroutine(player.TakeDame(dameEnemy));
    }
}
