﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameCtroler : MonoBehaviour
{
    public GameObject gameOverPopup;
    public GameObject player;
    public static GameCtroler Instance;

    public bool isGameOver = false;
    private Vector3 playerStartPosition;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        // Lưu vị trí ban đầu của player
        playerStartPosition = player.transform.position;
    }


    public void GameOver()
    {
        //if (!isGameOver)
        //{
            isGameOver = true;
            SavePlayerStartPosition();
        //}
    }

    private void SavePlayerStartPosition()
    {
        // Lưu vị trí ban đầu của player vào PlayerPrefs
        PlayerPrefs.SetFloat("PlayerStartPositionX", playerStartPosition.x);
        PlayerPrefs.SetFloat("PlayerStartPositionY", playerStartPosition.y);
        PlayerPrefs.SetFloat("PlayerStartPositionZ", playerStartPosition.z);
        PlayerPrefs.Save();
    }


    public void Continue()
    {
        // Tắt pop-up "Game Over"
        gameOverPopup.SetActive(false);

        // Load lại trạng thái ban đầu của scene từ PlayerPrefs
        float startX = PlayerPrefs.GetFloat("PlayerStartPositionX");
        float startY = PlayerPrefs.GetFloat("PlayerStartPositionY");
        float startZ = PlayerPrefs.GetFloat("PlayerStartPositionZ");

        // Đặt lại vị trí của player
        player.transform.position = new Vector3(startX, startY, startZ);

        // Đặt lại trạng thái game
        isGameOver = false;
    }

    public void RestartGame()
    {
        // Xóa PlayerPrefs
        PlayerPrefs.DeleteKey("PlayerStartPositionX");
        PlayerPrefs.DeleteKey("PlayerStartPositionY");
        PlayerPrefs.DeleteKey("PlayerStartPositionZ");
        PlayerPrefs.Save();

        // Load lại scene hiện tại
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
