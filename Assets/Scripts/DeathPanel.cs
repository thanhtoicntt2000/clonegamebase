﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPanel : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnEnable()
    {
        PlayerControllerTest.OnPlayerDeath += ShowGameOverPopup;
    }

    private void OnDisable()
    {
        PlayerControllerTest.OnPlayerDeath -= ShowGameOverPopup;
    }


    private void ShowGameOverPopup()
    {
        GetComponent<PlayerControllerTest>().gameOverUI.SetActive(true);
    }


    public void OnRestartButtonClicked()
    {
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        
    }

    public void OnMainMenuButtonClicked()
    {
        if (!GameCtroler.Instance.isGameOver)
            Application.Quit();
    }
}
