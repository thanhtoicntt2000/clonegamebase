﻿using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public Transform player;
    private Vector3 lastCheckpointPosition;

    private void Start()
    {
        // Lấy vị trí checkpoint gần nhất từ PlayerPrefs
        lastCheckpointPosition = new Vector3(
            PlayerPrefs.GetFloat("LastCheckpointX"),
            PlayerPrefs.GetFloat("LastCheckpointY"),
            PlayerPrefs.GetFloat("LastCheckpointZ")
        );
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Lưu vị trí checkpoint mới
            Debug.Log("You reached the checkpoint");
            lastCheckpointPosition = transform.position;

            // Lưu vị trí checkpoint gần nhất vào PlayerPrefs
            PlayerPrefs.SetFloat("LastCheckpointX", lastCheckpointPosition.x);
            PlayerPrefs.SetFloat("LastCheckpointY", lastCheckpointPosition.y);
            PlayerPrefs.SetFloat("LastCheckpointZ", lastCheckpointPosition.z);
            PlayerPrefs.Save();

            // Hủy checkpoint
            Destroy(gameObject);
        }
    }

    public void ResetPlayerToLastCheckpoint()
    {
        // Đặt vị trí của player về checkpoint gần nhất
        player.position = lastCheckpointPosition;
    }
}
