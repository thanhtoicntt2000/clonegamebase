using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireBall : MonoBehaviour
{
    public GameObject fireballGen;
    public GameObject fireballPos;
    PlayerControllerTest player;
    StatCtrl stat;
    float delayCast = 1f;
    [SerializeField] private AudioSource fireballSoundEffect;
    // Start is called before the first frame update
    void Start()
    {
        player = this.GetComponent<PlayerControllerTest>();
        stat = this.GetComponent<StatCtrl>();

    }

    // Update is called once per frame
    void Update()
    {
        delayCast -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.K) && player.currentMana >= 20 && delayCast <= 0)
        {
            fireballSoundEffect.Play();
            delayCast = 1f;
            player.currentMana -= 20;
            player.statCtrl.SetMana(player.currentMana);
            player.anim.SetTrigger("Cast");
            GameObject fireball = Instantiate(fireballGen);
            if (player.gameObject.transform.localScale.x < 0)
            {
                fireball.GetComponent<FireBall>().dir = Vector2.left;
            }
            else
            {
                fireball.GetComponent<FireBall>().dir = Vector2.right;
            }
            fireball.transform.position = fireballPos.transform.position;
        }
    }

}
