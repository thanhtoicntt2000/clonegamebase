using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellDame : MonoBehaviour
{

    public Animator animator;
    Rigidbody2D rig;
    Transform player;
    public float speed = 10f;
    public float offset = 3f;
    public bool allowAttack = false;
    float Spellflw = 0;
    [SerializeField] private Transform PointAtack;

    public LayerMask playerLayers;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rig = animator.GetComponent<Rigidbody2D>();
    }

    public void ActiveSpell()
    {
        // animator.SetTrigger("Attack");
        animator.ResetTrigger("Attackspell");// SetTrigger("Attack");
        this.gameObject.SetActive(true);
        this.transform.position = new Vector2(player.position.x, player.position.y + offset);
        allowAttack = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (allowAttack)
        {
            Spellflw += Time.deltaTime;
            if (Spellflw <= 3)
            {
                Vector2 target = new Vector2(player.position.x, player.position.y + offset);
                Vector2 newPos = Vector2.MoveTowards(rig.position, target, speed * Time.deltaTime);
                rig.MovePosition(newPos);
            }
            else if (Spellflw > 3)
            {
                StartCoroutine(DestroySpell());
            }
        }
    }


    IEnumerator DestroySpell()
    {
        animator.SetTrigger("Attack");
        allowAttack = false;
        yield return new WaitForSeconds(1f);
        Spellflw = 0;
        this.gameObject.SetActive(false);
    }

    public void spellDame()
    {
        Debug.Log("Spell!!!!");
        //tru mau player + anim player hurt

        Collider2D hitPlayer = Physics2D.OverlapCircle(PointAtack.position, 0.5f, playerLayers);
        PlayerControllerTest player = hitPlayer.GetComponent<PlayerControllerTest>();
        if (player != null) player.TakeDameImg(10);

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(PointAtack.position, 0.5f);
    }

    public void HideSoell()
    {
        this.gameObject.SetActive(false);

    }
}
