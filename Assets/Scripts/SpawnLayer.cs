using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLayer : MonoBehaviour
{
    private Vector2 startPos;
    private Vector2 PointSpawnPlayer;
    // Start is called before the first frame update

    private void Start()
    {
        startPos = transform.position;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Deadzone"))
        {
            Die();
            startPos = Vector2.zero;
        }
        if (collision.CompareTag("CheckPoint"))
        {
            Debug.Log("You reach the checkpoint!!!!");
            PointSpawm savepoint = new PointSpawm(collision.gameObject.transform.position);
            PlayerPrefs.SetString("CheckpoinPlayer", JsonUtility.ToJson(savepoint));
        }
    }


    void Die()
    {
        StartCoroutine(Respawn(1f));
    }

    public IEnumerator Respawn(float duration)
    {
        string point = PlayerPrefs.GetString("CheckpoinPlayer");
        PointSpawm cvPoint = JsonUtility.FromJson<PointSpawm>(point);
        yield return new WaitForSeconds(duration);
        if (cvPoint == null)
        {
            transform.position = startPos;
        }
        else
        {
            transform.position = cvPoint.point;

        }

    }

}
public class PointSpawm
{
    public Vector2 point;
    public PointSpawm(Vector2 point)
    {
        this.point = point;
    }
}
