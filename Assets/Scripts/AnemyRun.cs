using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnemyRun : StateMachineBehaviour
{
    //  OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public float speed = 2f;
    public float atackRange = 1f;
    public Transform Player;
    public Rigidbody2D rid;
    public EnemyMovement enemy;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        rid = animator.GetComponent<Rigidbody2D>();
        enemy = animator.GetComponent<EnemyMovement>();

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Player != null) enemy.Flip(Player);

        Vector2 target = new Vector2(Player.position.x, rid.position.y);
        Vector2 newPos = Vector2.MoveTowards(rid.position, target, speed * Time.deltaTime);
        rid.MovePosition(newPos);
        if (Vector2.Distance(Player.position, rid.position) <= atackRange)
        {
            animator.SetTrigger("Atack");
        }
    }

    //  OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    //// OnStateMove is called right after Animator.OnAnimatorMove()
    ////override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    ////{
    ////    // Implement code that processes and affects root motion
    ////}

    //// OnStateIK is called right after Animator.OnAnimatorIK()
    ////override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    ////{
    ////    // Implement code that sets up animation IK (inverse kinematics)
    ////}
}
