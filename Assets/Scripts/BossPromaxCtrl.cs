using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossPromaxCtrl : MonoBehaviour
{
    #region Variables
    // 
    public float attackRange = 1;
    public int dameBoss = 20;
    [SerializeField] private Rigidbody2D rig;
    [SerializeField] private Animator anim;
    [SerializeField] private int maxHP = 1000;
    private int currentHP = 0;

    [SerializeField] private Transform attackPoint;
    [SerializeField] private Transform attackPointTransform;

    // public float speed = 5;

    [SerializeField] private HeathCtrl _HeathCtrl;

    [SerializeField] private LayerMask playerLayers;

    private GameCtroler _gameCtroler;

    public bool alowTackDame = true;

    #endregion

    public void TransformBOSS()
    {
        attackPoint.gameObject.SetActive(false);
        attackPointTransform.gameObject.SetActive(true);

    }

    private void Start()
    {
        currentHP = maxHP;
        _HeathCtrl.SetHeath(currentHP, maxHP);
        _gameCtroler = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCtroler>();
    }

    public void Attack()
    {
        Transform temp = attackPoint.gameObject.activeInHierarchy == true ? attackPoint : attackPointTransform;
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(temp.position, attackRange, playerLayers);
        foreach (var item in hitPlayer)
        {
            StartCoroutine(item.GetComponent<PlayerControllerTest>().TakeDame(dameBoss));
        }
    }
    public void TakeDamage(int dame)
    {
        Debug.Log(alowTackDame + " ====== " + dame);
        if (!alowTackDame) return;
        currentHP -= dame;
        anim.SetTrigger("Hurt");
        _HeathCtrl.SetHeath(currentHP, maxHP);
        if (currentHP <= 600)
        {
            anim.SetBool("Transform", true);
        }
        if (currentHP <= 0)
        {
            Die();

        }
    }
    public void Die()
    {
        anim.SetBool("BossDie", true);
        StartCoroutine(LoadGameOverScene());
    }
    private IEnumerator LoadGameOverScene()
    {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("Over");
    }

    public void Flip(Transform TransformPlayer)
    {
        if (transform.position.x > TransformPlayer.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    private void OnDrawGizmos()
    {
        Transform temp = attackPoint.gameObject.activeInHierarchy == true ? attackPoint : attackPointTransform;
        Gizmos.DrawWireSphere(temp.position, attackRange);
    }
}

