using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{
    public Text textComponent;
    public float moveSpeed = 1f;
    public float destroyDelay = 1f;

    private void Start()
    {
        Destroy(gameObject, destroyDelay);
    }

    private void Update()
    {
        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
    }

    public void SetText(string text)
    {
        textComponent.text = text;
    }
}
